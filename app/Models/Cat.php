<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    protected $fillable = ['name', 'age', 'description'];

    public function pets(){
        return $this->belongsToMany(Cat::class, 'pets_catdog');

    }
}
