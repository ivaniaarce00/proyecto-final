<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{

    protected $fillable = ['name'];
    public  $timestamps = false;

    public function dogs() {
        return $this->belongsTo(Dog::class, 'pets_catdog');
    }

    public function cats() {
        return $this->belongsTo(Cat::class, 'pets_catdog');
    }
}
