<?php

namespace App\Http\Controllers;

use App\Models\Cat;
use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class CatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $cats = Cat::with('pets')->get();
        $pets = Pet::all();

        return Inertia::render('Cats/Index', [
            'cats' => $cats,
            'pets' => $pets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Cats/Create', [
            'pets' => Pet::all(),

        ]);
    }

    public function uploadImage()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $cat = new Cat();
        $cat->name = $request->name;
        $cat->age = $request->age;
        $cat->description = $request->description;

        $destino = 'img/cats';
        $image = $request->hasFile('image');
        if ($image) {
            $imageFile = $request->file('image');
            $filename = $request->name . '_'. $imageFile->getClientOriginalExtension();
            $imageFile->move($destino, $filename);
            $cat->image = $destino . '/' . $filename;
        }

        $cat->save();

        $cat->pets()->sync($request->pets);
        DB::commit();

        return redirect('/cats');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function show($id)
    {
        $cat = Cat::with( 'pets')->findOrFail($id);
        return Inertia::render('Cats/Show', [
            'cat' => $cat
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {

        $cat = Cat::with( 'pets')->findOrFail($id);

        return Inertia::render('Cats/Edit', [
            'cat' => $cat,
            'pets' => Pet::all(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $cat = Dog::findOrFail($id);
        $cat->name = $request->name;
        $cat->age = $request->age;
        $cat->description = $request->description;

        $pic = $request->file('file');
        $destino = 'img/cat';
        if ($pic) {
            $this->removeDogImage($cat);
            $pic = $request->file('file');
            $filename = $request->name . '_' . $pic->getClientOriginalExtension();
            $pic->move($destino. $filename);
            $cat->image = $destino . '/' . $filename;
        }

        $cat->save();
        $cat->pets()->sync($request->pets);
        DB::commit();

        return redirect('/cats');
    }

    private function removeDogImage(Cat $cat)
    {
        if (!empty($cat->image) && file_exists(public_path($cat->image))) {
            unlink(public_path($cat->image));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $cat = Cat::findOrFail($id);
        $cat->pets()->detach();
        $cat->delete();


        DB::commit();

        return back();
    }
}
