<?php

namespace App\Http\Controllers;

use App\Models\Dog;
use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class DogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $dogs = Dog::with('pets')->get();
        $pets = Pet::all();

        return Inertia::render('Dogs/Index', [
            'dogs' => $dogs,
            'pets' => $pets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Dogs/Create', [
            'pets' => Pet::all(),

        ]);
    }

    public function uploadImage()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $dog = new Dog();
        $dog->name = $request->name;
        $dog->age = $request->age;
        $dog->description = $request->description;

        $destino = 'img/dog';
        $image = $request->hasFile('image');
        if ($image) {
            $imageFile = $request->file('image');
            $filename = $request->name . '_'. $imageFile->getClientOriginalExtension();
            $imageFile->move($destino, $filename);
            $dog->image = $destino . '/' . $filename;
        }

        $dog->save();

        $dog->pets()->sync($request->pets);
        DB::commit();

        return redirect('/dogs');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function show($id)
    {
        $dog = Dog::with( 'pets')->findOrFail($id);
        return Inertia::render('Dogs/Show', [
            'dog' => $dog
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $dog = Dog::with( 'pets')->findOrFail($id);


        return Inertia::render('Dogs/Edit', [
            'dog' => $dog,
            'pets' => Pet::all(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $dog = Dog::findOrFail($id);
        $dog->name = $request->name;
        $dog->age = $request->age;
        $dog->description = $request->description;

        $pic = $request->file('file');
        $destino = 'img/dog';
        if ($pic) {
            $this->removeDogImage($dog);
            $pic = $request->file('file');
            $filename = $request->name . '_' . $pic->getClientOriginalExtension();
            $pic->move($destino. $filename);
            $dog->image = $destino . '/' . $filename;
        }

        $dog->save();
        $dog->pets()->sync($request->pets);
        DB::commit();

        return redirect('/dogs');
    }

    private function removeDogImage(Dog $dog)
    {
        if (!empty($dog->image) && file_exists(public_path($dog->image))) {
            unlink(public_path($dog->image));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $dog = Dog::findOrFail($id);
        $dog->pets()->detach();
        $dog->delete();


        DB::commit();

        return back();
    }
}
