(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      url: 'https://pbs.twimg.com/media/EengpTDWsAgOHS0?format=jpg&name=small',
      fits: ['scale-down'],
      src: 'https://s1.1zoom.me/b5050/691/Dogs_Cats_White_background_Puppy_562998_2560x1440.jpg',
      loginDialogVisible: false,
      log_form: {
        username: '',
        password: ''
      },
      log_rules: {
        username: {
          required: true,
          message: 'Su suario es requerido'
        },
        password: {
          required: true,
          message: 'Su contraseña es requerida'
        }
      },
      registerDialogVisible: false,
      reg_form: {
        name: '',
        username: '',
        password: '',
        password_confirmation: ''
      },
      reg_rules: {
        name: {
          required: true,
          message: 'Su nombre es requerido'
        },
        username: {
          required: true,
          message: 'Su suario es requerido'
        },
        password: {
          required: true,
          message: 'Su contraseña es requerida'
        }
      },
      menuItems: []
    };
  },
  methods: {
    login: function login() {
      var _this = this;

      this.$refs.log_form.validate(function () {
        _this.$inertia.post('/login', {
          username: _this.log_form.username,
          password: _this.log_form.password
        });
      });
    },
    register: function register() {
      var _this2 = this;

      this.$refs.reg_form.validate(function () {
        _this2.$inertia.post('/register', {
          name: _this2.reg_form.name,
          username: _this2.reg_form.username,
          password: _this2.reg_form.password,
          password_confirmation: _this2.reg_form.password_confirmation
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "el-header",
        { staticClass: "login-header" },
        [
          _c("el-row", [
            _c("div", { staticClass: "demo-image__placeholder" }, [
              _c(
                "div",
                { staticClass: "block" },
                [
                  _c("span", { staticClass: "demonstration" }),
                  _vm._v(" "),
                  _c("el-image", { attrs: { fit: "scale-dow", src: _vm.url } })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "header-right" },
              [
                _c(
                  "el-link",
                  {
                    staticStyle: {
                      color: "black",
                      "margin-top": "60px",
                      "font-weight": "bold"
                    },
                    attrs: { underline: false },
                    on: {
                      click: function($event) {
                        _vm.loginDialogVisible = true
                      }
                    }
                  },
                  [_vm._v("Login")]
                )
              ],
              1
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c("el-header", {
        staticStyle: { height: "30px", "background-color": "#EDE7F6" }
      }),
      _vm._v(" "),
      _c(
        "el-menu",
        {
          staticStyle: { "font-size": "30px", height: "56px" },
          attrs: {
            mode: "horizontal",
            "background-color": "#f5f7fa",
            "text-color": "black",
            "active-text-color": "black"
          }
        },
        [
          _c("el-menu-item", { attrs: { index: "1" } }, [
            _c("a", { attrs: { href: "/about" } }, [
              _c("strong", [_vm._v("Conoce acerca de nosotros")])
            ])
          ]),
          _vm._v(" "),
          _c("el-menu-item", { attrs: { index: "2" } }, [
            _c("a", { attrs: { href: "/contact" } }, [
              _c("strong", [_vm._v("Contacto")])
            ])
          ]),
          _vm._v(" "),
          _c(
            "el-dropdown",
            { attrs: { index: "3", trigger: "click" } },
            [
              _c(
                "el-button",
                {
                  staticClass: "el-menu-item",
                  staticStyle: { "background-color": "#f5f7fa" },
                  attrs: { icon: "el-icon-arrow-down" }
                },
                [_c("strong", [_vm._v("Mascotas disponibles")])]
              ),
              _vm._v(" "),
              _c(
                "el-dropdown-menu",
                { attrs: { slot: "dropdown" }, slot: "dropdown" },
                [
                  _c(
                    "a",
                    { attrs: { href: "/dogy" } },
                    [
                      _c("el-dropdown-item", [_c("strong", [_vm._v("Perros")])])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "a",
                    { attrs: { href: "/caty" } },
                    [_c("el-dropdown-item", [_c("strong", [_vm._v("Gatos")])])],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "Login", visible: _vm.loginDialogVisible },
          on: {
            "update:visible": function($event) {
              _vm.loginDialogVisible = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "log_form",
              attrs: { model: _vm.log_form, rules: _vm.log_rules }
            },
            [
              _c(
                "el-form-item",
                { attrs: { label: "Usuario", prop: "username" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Usuario" },
                    model: {
                      value: _vm.log_form.username,
                      callback: function($$v) {
                        _vm.$set(_vm.log_form, "username", $$v)
                      },
                      expression: "log_form.username"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Contraseña", prop: "password" } },
                [
                  _c("el-input", {
                    attrs: {
                      type: "password",
                      "show-password": "",
                      placeholder: "Contraseña"
                    },
                    model: {
                      value: _vm.log_form.password,
                      callback: function($$v) {
                        _vm.$set(_vm.log_form, "password", $$v)
                      },
                      expression: "log_form.password"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.$page.prop.errors
                ? _c(
                    "div",
                    _vm._l(_vm.$page.prop.errors, function(error) {
                      return _c(
                        "div",
                        _vm._l(error, function(er, index) {
                          return _c("el-alert", {
                            key: index,
                            staticStyle: { "margin-bottom": "5px" },
                            attrs: { type: "error", title: er, closable: false }
                          })
                        }),
                        1
                      )
                    }),
                    0
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                { staticStyle: { "text-align": "center" } },
                [
                  _c(
                    "el-button",
                    {
                      attrs: { type: "primary" },
                      on: {
                        click: function($event) {
                          return _vm.login()
                        }
                      }
                    },
                    [_vm._v("Entrar")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "demo-image__placeholder" }, [
        _c(
          "div",
          { staticClass: "block" },
          [
            _c("span", { staticClass: "demonstration" }),
            _vm._v(" "),
            _c("el-image", { attrs: { src: _vm.src } })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue":
/*!*******************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=a2ac2cea& */ "./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Auth/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=a2ac2cea& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);