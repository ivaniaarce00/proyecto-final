<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    Route::resource('/dogs', 'DogController');
    Route::post('/dogs/{id}', 'DogController@update');
    Route::post('/cats/{id}', 'CatController@update');
    Route::resource('/cats', 'CatController');
    Route::resource('/pets', 'PetController');



});

Route::resource('/about', 'AboutController');
Route::resource('/contact', 'ContactController');
Route::resource('/dogy', 'DogyController');
Route::resource('/caty', 'CatyController');


