<?php

use Illuminate\Database\Seeder;
use App\Models\Pet;

class PetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Pet::insert([
         [
             "name" => "Perro"
         ],

           [
              "name" => "Gato"
           ]
       ]);
    }
}
