<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BaseMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('pets', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
        });



        Schema::create('dogs', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('age');
            $table->string('description');
            $table->string('image')->nullable();
            $table->timestamps();


        });

        Schema::create('cats', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('age');
            $table->string('description');
            $table->string('image')->nullable();
            $table->timestamps();


        });

        Schema::create('pets_catdog', function (Blueprint $table) {
            $table->integer('dog_id')->unsigned();
            $table->integer('cat_id')->unsigned();
            $table->integer('pets_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('cats');
            $table->foreign('dog_id')->references('id')->on('dogs');
            $table->foreign('pets_id')->references('id')->on('pets');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('pets');
        Schema::dropIfExists('dogs');
        Schema::dropIfExists('cats');
        Schema::dropIfExists('pets_catdog');
    }
}
